//
//  RealmManager.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 24.04.2021.
//

import Foundation
import RealmSwift

protocol RealmConvertible {
    associatedtype RealmModelType
    func toRealmModel() -> RealmModelType
}

protocol PlainConvertible {
    associatedtype PlainModelType
    func toPlainModel() -> PlainModelType
}

class RealmManager {
    
    func write<T: Object>(object: [T]) {
        let realm: Realm = try! Realm()
        try! realm.write {
            realm.add(object, update: .modified)
        }
    }
    
    func obtain<T: Object>(objectType: T.Type) -> Results<T>? {
        let realm: Realm = try! Realm()
        return realm.objects(objectType)
    }
    
    func delete<T: Object>(objects: [T]) {
        let realm: Realm = try! Realm()
        try! realm.write {
            realm.delete(objects)
        }
    }
}
