//
//  CommonViews.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 29.04.2021.
//

import SwiftUI

class CommonViews {
    
    static func greenBackground() -> some View {
        // Делаем задник зеленым градиентом
        let backgroundView = LinearGradient(gradient: Gradient(colors: [Color(.sRGB, red: 0.922, green: 0.961, blue: 0.929, opacity: 1), Color(.sRGB, red: 0.875, green: 0.949, blue: 0.941, opacity: 1)]), startPoint: .leading, endPoint: .trailing)
        
        return backgroundView
            .edgesIgnoringSafeArea(.top) // Растягиваем задник наверх, игнорируя safeArea
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/) // Растягиваем задник по всей ширине и высорте экрана
    }
    
    static func grayBackground() -> some View {
        return Color.gray.opacity(0.4)
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height, alignment: .center)
            .edgesIgnoringSafeArea(.all)
    }
    
    static func border(color: Color, cornerRadius: CGFloat, width: CGFloat) -> some View {
        // накладывается на вьюху через .overlay
        RoundedRectangle(cornerRadius: cornerRadius).stroke(color, lineWidth: width)
    }
}
