//
//  Images.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 28.04.2021.
//

import SwiftUI
class Images {
    static let food = "Food"
    static let entertainment = "Entertainment"
    
    static let beauty = "Beauty"
    static let bills = "Bills"
    static let children = "Children"
    static let health = "Health"
    static let home = "Home"
    static let loans = "Loans"
    static let misc = "Misc"
    static let pets = "Pets"
    static let savings = "Savings"
    static let shopping = "Shopping"
    static let transport = "Transport"
    static let trips = "Trips"

    
    static let close = "Close"
    static let handWithMoney = "HandWithMoney"
    
    static let comment = "Comment"
}
