//
//  Colors.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 27.04.2021.
//

import SwiftUI
class Colors {
    static let primaryTextColor = Color(.sRGB, red: 0.29, green: 0.308, blue: 0.294, opacity: 1)
    static let secondaryTextColor = Color(.sRGB, red: 0.573, green: 0.6, blue: 0.579, opacity: 1)
}
