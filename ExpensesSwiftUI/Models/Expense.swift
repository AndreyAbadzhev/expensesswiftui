//
//  ExpenseModel.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 31.03.2021.
//

import Foundation
import RealmSwift

struct Expense: Identifiable, RealmConvertible, Equatable {
    
    typealias RealmModelType = ExpenseRealm
    
    var id: String
    
    var category: ExpenseCategory
    var summ: Int
    var comment: String
    
    init(expenseRealm: ExpenseRealm) {
        self.id = expenseRealm.id
        self.category = ExpenseCategory(rawValue: expenseRealm.categoryId) ?? .unknown
        self.summ = expenseRealm.summ
        self.comment = expenseRealm.comment
    }
    
    init(category: ExpenseCategory, summ: Int, comment: String) {
        self.id = UUID().uuidString
        self.category = category
        self.summ = summ
        self.comment = comment
    }
    
    func toRealmModel() -> ExpenseRealm {
        return ExpenseRealm(expense: self)
    }
}

enum ExpenseCategory: Int {
    case food
    case entertainment
    case beauty
    case bills
    case children
    case health
    case home
    case loans
    case misc
    case pets
    case savings
    case shopping
    case transport
    case trips
    
    case unknown
    
    var name: String {
        switch self {
        case .food: return "Еда"
        case .entertainment: return "Развлечения"
        case .beauty: return "Красота"
        case .bills: return "Счета"
        case .children: return "Дети"
        case .health: return "Здоровье"
        case .home: return "Дом"
        case .loans: return "Кредиты"
        case .misc: return "Разное"
        case .pets: return "Животные"
        case .savings: return "Накопления"
        case .shopping: return "Шоппинг"
        case .transport: return "Транспорт"
        case .trips: return "Путешествия"
        case .unknown: return "Неизвестно"
        }
    }
    
    var imageName: String {
        switch self {
        case .food: return Images.food
        case .entertainment: return Images.entertainment
        case .beauty: return Images.beauty
        case .bills: return Images.bills
        case .children: return Images.children
        case .health: return Images.health
        case .home: return Images.home
        case .loans: return Images.loans
        case .misc: return Images.misc
        case .pets: return Images.pets
        case .savings: return Images.savings
        case .shopping: return Images.shopping
        case .transport: return Images.transport
        case .trips: return Images.trips
        case .unknown: return "Неизвестно"
        }
    }
    
    init(from decoder: Decoder) throws {
        self = try ExpenseCategory(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .unknown
    }
}

class ExpenseRealm: Object, PlainConvertible {
    
    typealias PlainModelType = Expense
    
    @objc dynamic var id: String = ""
    @objc dynamic var categoryId: Int = 0
    @objc dynamic var summ: Int = 0
    @objc dynamic var comment: String = ""
    
    convenience init(expense: Expense) {
        self.init()
        self.id = expense.id
        self.categoryId = expense.category.rawValue
        self.summ = expense.summ
        self.comment = expense.comment
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    func toPlainModel() -> Expense {
        return Expense(expenseRealm: self)
    }
}
