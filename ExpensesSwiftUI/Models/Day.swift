//
//  DayExpensesModel.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 31.03.2021.
//

import Foundation
import RealmSwift

struct Day: Identifiable, RealmConvertible {
    
    typealias RealmModelType = DayRealm
    
    var id: String
            
    var dayNumber: Int
    var monthNumber: Int
    
    var expenses: [Expense] {
        didSet {
            // каждый раз при изменении массива expenses (где лежат все траты) надо пересчитывать массив expensesReducedByCategories, потому что к нему биндится отображение дня в списке месяцев
            calculateExpensesReducedByCategories()
        }
    }
    
    
    var expensesReducedByCategories: [Expense] = []
    
    var totalExpensesSumm: Int {
        return expenses.map { $0.summ }.reduce(0, +)
    }
    
    var dateString: String {
        let monthName: String
        
        switch monthNumber {
        case 1: monthName = "Января"
        case 2: monthName = "Февраля"
        case 3: monthName = "Марта"
        case 4: monthName = "Апреля"
        case 5: monthName = "Мая"
        case 6: monthName = "Июня"
        case 7: monthName = "Июля"
        case 8: monthName = "Августа"
        case 9: monthName = "Сентября"
        case 10: monthName = "Октября"
        case 11: monthName = "Ноября"
        case 12: monthName = "Декабря"
        default: return "Неверно указана дата"
        }
        
        return "\(dayNumber) \(monthName)"
    }

    init(monthNumber: Int, dayNumber: Int, expenses: [Expense]) {
        self.id = UUID().uuidString
        self.monthNumber = monthNumber
        self.dayNumber = dayNumber
        self.expenses = expenses
        // при инициализации тоже надо высчитывать массив expensesReducedByCategories
        calculateExpensesReducedByCategories()
    }
    
    init(dayRealm: DayRealm) {
        self.id = dayRealm.id
        self.monthNumber = dayRealm.monthNumber
        self.dayNumber = dayRealm.dayNumber
        self.expenses = dayRealm.expenses.map { $0.toPlainModel() }
        // при инициализации тоже надо высчитывать массив expensesReducedByCategories
        calculateExpensesReducedByCategories()
    }
    
    mutating func calculateExpensesReducedByCategories() {
        // эта функция для изменения массива expensesReducedByCategories нужна потому, что забиндить вьюху можно только к обычной переменной, которая не имеет собственных мутабельных геттеров и сеттеров. expensesReducedByCategories - обычная переменная. И к ней забиндены траты во вьюхе одного дня в списке месяцев. Но при этом эта переменная должна меняться в зависимости от массива expenses. Поэтому нужна мутабельная функция, которая будет менять переменную expensesReducedByCategories в зависимости от переменной expenses, не задавая самой expensesReducedByCategories геттеров и сеттеров (из-за которых к ней нельзя было бы забиндить вьюху).
        var expensesReducedByCategories = [Expense]()

        let usedCategories = Set(expenses.map { $0.category })
        usedCategories.forEach { category in
            let categoryTotalSumm = expenses.filter { $0.category == category }.reduce(0, { $0 + $1.summ })
            expensesReducedByCategories.append(Expense(category: category, summ: categoryTotalSumm, comment: ""))
        }

        self.expensesReducedByCategories = expensesReducedByCategories.sorted(by: { $0.category.rawValue < $1.category.rawValue })
    }
    
    func toRealmModel() -> DayRealm {
        return DayRealm(day: self)
    }
}

class DayRealm: Object, PlainConvertible {
    
    typealias PlainModelType = Day
    
    @objc dynamic var id: String = ""
    @objc dynamic var dayNumber: Int = 0
    @objc dynamic var monthNumber: Int = 0
    
    dynamic var expenses = List<ExpenseRealm>()
    
    convenience init(day: Day) {
        self.init()
        self.id = day.id
        self.dayNumber = day.dayNumber
        self.monthNumber = day.monthNumber
        self.expenses.append(objectsIn: day.expenses.map { $0.toRealmModel() })
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    func toPlainModel() -> Day {
        return Day(dayRealm: self)
    }

}
