//
//  MonthExpenses.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 03.04.2021.
//

import Foundation
import RealmSwift

struct Month: Identifiable, RealmConvertible {
    
    typealias RealmModelType = MonthRealm
    
    var id: String
    
    var monthNumber: Int
    var yearNumber: Int
    
    var days: [Day]
    
    var monthName: String {
        
        switch monthNumber {
        case 1: return "Январь - \(yearNumber)"
        case 2: return "Февраль - \(yearNumber)"
        case 3: return "Март - \(yearNumber)"
        case 4: return "Апрель - \(yearNumber)"
        case 5: return "Май - \(yearNumber)"
        case 6: return "Июнь - \(yearNumber)"
        case 7: return "Июль - \(yearNumber)"
        case 8: return "Август - \(yearNumber)"
        case 9: return "Сентябрь - \(yearNumber)"
        case 10: return "Октябрь - \(yearNumber)"
        case 11: return "Ноябрь - \(yearNumber)"
        case 12: return "Декабрь - \(yearNumber)"
        default: return "Неверно указана дата"
        }
    }
    
    var totalExpensesSumm: Int {
        return days.map { $0.totalExpensesSumm }.reduce(0, +)
    }
    
    init(monthRealm: MonthRealm) {
        self.id = monthRealm.id
        self.monthNumber = monthRealm.monthNumber
        self.yearNumber = monthRealm.yearNumber
        self.days = monthRealm.days.map { $0.toPlainModel() }
    }
    
    init(yearNumber: Int, monthNumber: Int, days: [Day]) {
        self.id = UUID().uuidString
        self.yearNumber = yearNumber
        self.monthNumber = monthNumber
        self.days = days
    }
    
    func toRealmModel() -> MonthRealm {
        return MonthRealm(month: self)
    }
}

class MonthRealm: Object, PlainConvertible {
    
    typealias PlainModelType = Month
    
    @objc dynamic var id: String = ""
    
    @objc dynamic var monthNumber: Int = 0
    @objc dynamic var yearNumber: Int = 0
    
    dynamic var days = List<DayRealm>()
    
    convenience init(month: Month) {
        self.init()
        self.id = month.id
        self.monthNumber = month.monthNumber
        self.yearNumber = month.yearNumber
        self.days.append(objectsIn: month.days.map { $0.toRealmModel() })
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    func toPlainModel() -> Month {
        return Month(monthRealm: self)
    }
}
