//
//  AddExpenseView.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 24.04.2021.
//

import SwiftUI

struct AddExpenseView: View {
    
    @ObservedObject var viewModel: AddExpenseViewModel
    
    var screenWidth: CGFloat = UIScreen.main.bounds.width
    
    var numberOfCellsInLine = 3
    
    var cellsLineLeftAndRightInset: CGFloat = 30
    
    var interCellsSpace: CGFloat = 6
    
    var cellSize: CGFloat {
        let screenWidthWithoutLeftAndRightInsets = screenWidth - cellsLineLeftAndRightInset * 2
        let screenWidthWithoutLeftAndRightInsetsAndWithoutInterCellsSpace = screenWidthWithoutLeftAndRightInsets - (interCellsSpace * CGFloat((numberOfCellsInLine - 1)))
        let cellSize = screenWidthWithoutLeftAndRightInsetsAndWithoutInterCellsSpace / CGFloat(numberOfCellsInLine)
        return cellSize
    }
    
    var interSectionSpace: CGFloat = 24
    
    let categoriesArray: [ExpenseCategory] = [.food, .entertainment, .beauty, .bills, .children, .health, .home, .loans, .misc, .pets, .savings, .shopping, .transport, .trips]
    
    private let threeColumnGrid = [
            GridItem(.flexible(minimum: 40), spacing: 6),
            GridItem(.flexible(minimum: 40), spacing: 6),
            GridItem(.flexible(minimum: 40), spacing: 6),
        ]
    
    var body: some View {
        
        VStack {
            Divider()
            ScrollView(.vertical, showsIndicators: false) {
                
                // Аналог CollectionView
                LazyVGrid(columns: threeColumnGrid, alignment: .center, spacing: 0) {
                    ForEach(categoriesArray.indices) { index in
                        CategoryCell(category: categoriesArray[index]).frame(width: cellSize, height: cellSize).onTapGesture {
                            viewModel.expenseCategoryDidSelect(category: categoriesArray[index])
                        }
                    }
                }
            }
            .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            .navigationBarTitle("Категории", displayMode: .inline)
            .navigationBarItems(trailing: Button(action: {
                viewModel.closeButtonDidTap()
            }, label: {
                Image(Images.close).resizable().frame(width: 28, height: 28).foregroundColor(Colors.primaryTextColor)
            }))
        }
        .animation(.default)
        .blur(radius: viewModel.inputSummViewIsShown ? 2 : 0)
        .background(LinearGradient(gradient: Gradient(colors: [Color(.sRGB, red: 0.992, green: 0.984, blue: 0.984, opacity: 1), Color(.sRGB, red: 0.922, green: 0.929, blue: 0.933, opacity: 1)]), startPoint: .leading, endPoint: .trailing).edgesIgnoringSafeArea(.all))
        .overlay(ExpenseInputView(expense: $viewModel.expenseToAdd, date: $viewModel.expenseDate, isDatePickerAvailable: true, closeButtonDidTap: {
            viewModel.inputSummViewIsShown = false
        }, doneButtonDidTap: {
            viewModel.doneButtonDidTap()
        }).opacity(viewModel.inputSummViewIsShown ? 1 : 0).animation(.default))
    }
}

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
