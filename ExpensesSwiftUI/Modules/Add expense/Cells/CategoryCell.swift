//
//  CategoryCell.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 28.04.2021.
//

import SwiftUI

struct CategoryCell: View {
    
    @State var category: ExpenseCategory
    
    var body: some View {
        VStack {
            Image(category.imageName).resizable().frame(width: 40, height: 40)
            Text(category.name).foregroundColor(Colors.primaryTextColor).font(.system(size: 13))
        }
    }
}
