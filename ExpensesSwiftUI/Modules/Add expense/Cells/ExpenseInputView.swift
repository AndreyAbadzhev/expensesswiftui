//
//  AddExpenseInputSummView.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 29.04.2021.
//

import SwiftUI

struct ExpenseInputView: View {
    
    var viewWidth: CGFloat = UIScreen.main.bounds.width - 32
    var viewHeight: CGFloat = (UIScreen.main.bounds.width - 32) / 1.5
    
    @Binding var expense: Expense
    @Binding var date: Date
    
    var isDatePickerAvailable: Bool
        
    var closeButtonDidTap: (() -> Void)?
    var doneButtonDidTap: (() -> Void)?
    
    var body: some View {
        
        VStack {
            
            HStack {
                Spacer().frame(width: 20) // этот спейсер с заданной шириной специально здесь для того, чтобы уравновесить кнопку "Закрыть", которая справа и тоже имеет ширину 20. Картинка категории и название категории в таком слечае оказываются ровно по центру, хотя справа кнопка "Закрыть", а слева ничего нет.
                Spacer()
                Image(expense.category.imageName)
                    .resizable()
                    .frame(width: 22.0, height: 22.0)
                Text(expense.category.name)
                    .foregroundColor(Colors.primaryTextColor)
                    .font(.system(size: 17))
                Spacer()
                Button(action: {
                    UIApplication.shared.endEditing()
                    closeButtonDidTap?()
                }, label: {
                    Image(Images.close)
                })
                .frame(width: 20, height: 20, alignment: .center)
            }.padding(EdgeInsets(top: 16, leading: 32, bottom: 0, trailing: 32))
            
            Spacer().frame(height: 32)
            
            if isDatePickerAvailable {
                HStack {
                    Text("Дата:")
                        .foregroundColor(Colors.primaryTextColor)
                        .font(.system(size: 17))
                    DatePicker("", selection: $date, in: ...Date(), displayedComponents: .date)
                        .labelsHidden()
                    Spacer()
                }.padding(EdgeInsets(top: 0, leading: 32, bottom: 0, trailing: 32))
                
                Spacer().frame(height: 16)
            }
            
            HStack {
                Text("Сумма:")
                    .foregroundColor(Colors.primaryTextColor)
                    .font(.system(size: 17))
                TextField("", text: Binding(
                    get: { String(expense.summ) },
                    set: { expense.summ = Int($0) ?? 0 }
                ))
                .foregroundColor(Colors.secondaryTextColor)
                .keyboardType(.numberPad)
                .padding(EdgeInsets(top: 4, leading: 8, bottom: 4, trailing: 8))
                .background(RoundedCorners(color: Color(.sRGB, red: 0.976, green: 0.976, blue: 0.976, opacity: 0.94), tl: 17, tr: 17, bl: 17, br: 17).overlay(CommonViews.border(color: Color(.sRGB, red: 0.463, green: 0.463, blue: 0.502, opacity: 0.12), cornerRadius: 17, width: 1)))
            }.padding(EdgeInsets(top: 0, leading: 32, bottom: 0, trailing: 32))
            
            Spacer().frame(height: 16)
            
            HStack {
                Text("Комментарий:")
                    .foregroundColor(Colors.primaryTextColor)
                    .font(.system(size: 17))
                TextField("Здесь пока ничего нет", text: $expense.comment)
                .foregroundColor(Colors.secondaryTextColor)
                .keyboardType(.default)
                .padding(EdgeInsets(top: 4, leading: 8, bottom: 4, trailing: 8))
                .background(RoundedCorners(color: Color(.sRGB, red: 0.976, green: 0.976, blue: 0.976, opacity: 0.94), tl: 17, tr: 17, bl: 17, br: 17).overlay(CommonViews.border(color: Color(.sRGB, red: 0.463, green: 0.463, blue: 0.502, opacity: 0.12), cornerRadius: 17, width: 1)))
            }.padding(EdgeInsets(top: 0, leading: 32, bottom: 0, trailing: 32))
            
            Spacer().frame(height: 16)
            
            Button("Готово", action: {
                UIApplication.shared.endEditing() // Снимаем firstResponder с TextField чтобы убрать клавиатуру. Как это сделать по-другому - фиг знает
                doneButtonDidTap?()
            })
            .foregroundColor(expense.summ == 0 ? Color.white : Colors.primaryTextColor)
            .font(.system(size: 15, weight: .semibold, design: .default))
            .padding(EdgeInsets(top: 8, leading: 24, bottom: 8, trailing: 24))
            .background(RoundedCorners(color: expense.summ == 0 ? Color.gray : Color(.sRGB, red: 0.875, green: 0.949, blue: 0.941, opacity: 0.94), tl: 20, tr: 20, bl: 20, br: 20))
            .disabled(expense.summ == 0)
            
            Spacer().frame(height: 16)
            
        }
        .background(RoundedCorners(color: .white, tl: 12, tr: 12, bl: 12, br: 12).padding(EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16)).background(CommonViews.grayBackground()))
    }
}
