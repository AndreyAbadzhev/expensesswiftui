//
//  AddExpenseViewModel.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 28.04.2021.
//

import SwiftUI
class AddExpenseViewModel: ObservableObject {
    
    weak var controller: AddExpenseController?
    
    var monthsBinding: Binding<[Month]>
    
    @Published var inputSummViewIsShown = false
    
    @Published var expenseToAdd = Expense(category: .unknown, summ: 0, comment: "")
    @Published var expenseDate = Date()
    
    init(monthsBinding: Binding<[Month]>) {
        self.monthsBinding = monthsBinding
    }
    
    func closeButtonDidTap() {
        controller?.closeModule()
    }
    
    func doneButtonDidTap() {
                
        let realmManager = RealmManager()
        
        let expenseYearNumber = Calendar.current.component(.year, from: expenseDate)
        let expenseMonthNumber = Calendar.current.component(.month, from: expenseDate)
        let expenseDayNumber = Calendar.current.component(.day, from: expenseDate)
        
        if !monthsBinding.wrappedValue.isEmpty {
            
            if let indexOfCompatibleMonth = monthsBinding.wrappedValue.firstIndex(where: { $0.yearNumber == expenseYearNumber && $0.monthNumber == expenseMonthNumber }) {
                
                
                if let indexOfCompatibleDay = monthsBinding.wrappedValue[indexOfCompatibleMonth].days.firstIndex(where: { $0.dayNumber == expenseDayNumber }) {
                    // Нашли месяц и день, в который надо положить расход
                    monthsBinding.wrappedValue[indexOfCompatibleMonth].days[indexOfCompatibleDay].expenses.append(expenseToAdd)
                    
                    realmManager.write(object: [monthsBinding.wrappedValue[indexOfCompatibleMonth].days[indexOfCompatibleDay].toRealmModel()])
                } else {
                // Нашли месяц, но не нашли день, в который надо положить расход
                    let newDay = Day(monthNumber: monthsBinding.wrappedValue[indexOfCompatibleMonth].monthNumber, dayNumber: expenseDayNumber, expenses: [expenseToAdd])
                    
                    monthsBinding.wrappedValue[indexOfCompatibleMonth].days.append(newDay)
                    monthsBinding.wrappedValue[indexOfCompatibleMonth].days = monthsBinding.wrappedValue[indexOfCompatibleMonth].days.sorted { $0.dayNumber > $1.dayNumber }
                    
                    realmManager.write(object: [monthsBinding.wrappedValue[indexOfCompatibleMonth].toRealmModel()])
                }
            } else {
                // Не нашли месяц, в который надо положить расход
                let newMonth = Month(yearNumber: expenseYearNumber, monthNumber: expenseMonthNumber, days: [Day(monthNumber: expenseMonthNumber, dayNumber: expenseDayNumber, expenses: [expenseToAdd])])
                
                monthsBinding.wrappedValue.append(newMonth)
                monthsBinding.wrappedValue = monthsBinding.wrappedValue.sorted {
                    $0.yearNumber != $1.yearNumber ? $0.yearNumber > $1.yearNumber : $0.monthNumber > $1.monthNumber
                }
                
                realmManager.write(object: [newMonth.toRealmModel()])
            }
        } else {
            // Месяцев вообще нет
            let newMonth = Month(yearNumber: expenseYearNumber, monthNumber: expenseMonthNumber, days: [Day(monthNumber: expenseMonthNumber, dayNumber: expenseDayNumber, expenses: [expenseToAdd])])
            monthsBinding.wrappedValue = [newMonth]
            
            realmManager.write(object: [newMonth.toRealmModel()])
        }

        controller?.closeModule()
    }
    
    func expenseCategoryDidSelect(category: ExpenseCategory) {
        expenseToAdd.category = category
        inputSummViewIsShown = true
    }
}
