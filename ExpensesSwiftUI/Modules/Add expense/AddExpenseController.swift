//
//  AddExpenseController.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 24.04.2021.
//

import SwiftUI
class AddExpenseController: UIHostingController<AddExpenseView> {

    init(monthsBinding: Binding<[Month]>) {
        let viewModel = AddExpenseViewModel(monthsBinding: monthsBinding)
        super.init(rootView: AddExpenseView(viewModel: viewModel))
        
        viewModel.controller = self
    }

    @objc required dynamic init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func closeModule() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
