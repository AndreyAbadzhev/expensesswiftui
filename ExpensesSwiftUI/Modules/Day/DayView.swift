//
//  DayView.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 03.04.2021.
//

import SwiftUI

struct DayView: View {
    
    @ObservedObject var viewModel: DayViewModel
    
    var body: some View {
        Divider().hidden()
        List {
            DayExpensesSectionHeaderView(title: nil, totalSumm: viewModel.day.totalExpensesSumm)
                .listRowBackground(Color.clear)
                .padding(EdgeInsets(top: 0, leading: -16, bottom: 0, trailing: -16))
            ForEach(viewModel.day.expenses.indices, id: \.self) { index in
                ShowExpenseCell(expense: viewModel.day.expenses[index], isCommentAvailable: true)
                .listRowBackground(Color.clear)
                .padding(EdgeInsets(top: 8, leading: 16, bottom: 8, trailing: 16))
                    .background(
                        RoundedCorners(color: .white, tl: 12, tr: 12, bl: 12, br: 12)
                            .shadow(color: Color.gray.opacity(0.3), radius: 3, x: 0, y: 5)
                            .onTapGesture {
                                viewModel.expenseDidSelect(expenseIndex: index)
                            }
                    )
            }.onDelete(perform: viewModel.deleteExpenseDidSwipe)
        }
        .animation(.default)
        .background(CommonViews.greenBackground())
        .navigationBarTitle(Text(viewModel.day.dateString), displayMode: .inline)
        
        .overlay(ExpenseInputView(expense: $viewModel.day.expenses[viewModel.selectedExpenseIndex ?? 0], date: $viewModel.date, isDatePickerAvailable: false, closeButtonDidTap: {
            viewModel.inputSummViewIsShown = false
        }, doneButtonDidTap: {
            viewModel.expeseEdittingDoneButtonDidTap()
        })
        .opacity(viewModel.inputSummViewIsShown ? 1 : 0)
        .animation(.default))
        
    }
}
