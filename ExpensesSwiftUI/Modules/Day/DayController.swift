//
//  DayController.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 03.04.2021.
//

import SwiftUI
class DayController: UIHostingController<DayView> {
    
    init(dayBinding: Binding<Day>) {
        let viewModel = DayViewModel(dayBinding: dayBinding)
        super.init(rootView: DayView(viewModel: viewModel))
        viewModel.controller = self
    }
    
    @objc required dynamic init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func closeModule() {
        self.navigationController?.popViewController(animated: true)
    }
}
