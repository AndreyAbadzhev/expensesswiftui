//
//  DayViewModel.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 26.04.2021.
//

import SwiftUI

class DayViewModel: ObservableObject {
    
    weak var controller: DayController?
            
    
    // Ссылка на день. Она является binding, и это значит, что любое изменение здесь повлечет за собой изменения этого объекта в предыдущих контроллерах
    let dayBinding: Binding<Day>
    @Published var date = Date() // не используется в этом модуле
    
    // День. Он является Published. Это значит, что любое измение этой переменной повлечет за собой перерисовку вьюх, подписанных на нее (в этом контроллере вьюха как раз подписана на эту переменную).
    @Published var day: Day {
        didSet {
            // При любом изменении в дне сраниваем, есть ли изменения по сравнению с тем днем, который нам достался по ссылке из предыдущих модулей. Если изменения есть, то ставим doneButtonEnabled true. В противном случае doneButtonEnabled ставим false
            doneButtonEnabled = day.expenses != dayBinding.wrappedValue.expenses
        }
    }
    
    @Published var inputSummViewIsShown = false
    @Published var selectedExpenseIndex: Int?
    
    // Состояние кнопки doneButton. По умолчанию оно false (кнопка недоступна). Если поменять значение этой переменной на true, то кнопка автоматически станет доступна, потому что на эту Published переменную подписана кнопка во вьюхе.
    @Published var doneButtonEnabled = false
    
    init(dayBinding: Binding<Day>) {
        // Ссылка на день, переданная нам из предыдущего модуля начинает храниться в этом контроллере, чтобы у нас была возможность в любой момент задать ей новое значение и спровоцировать перерисовку вьюх предыдущих модулей, завязанных на массив, в котором лежит этот объект.
        self.dayBinding = dayBinding
        
        // Из объекта по ссылке создаем новый объект, откопировав его свойства. Делаем это для того, чтобы изменять объект независимо от объектов на предыдущих модулях. Откопированный объект будем использовать для построения вьюх в текущем модуле. В будущем мы сможем передать значение этого независимого объекта по ссылке, чтобы спровоцировать перерисовку вьюх предыдущих модулей. Или не передавать, тогда никаких изменений не будет.
        self.day = dayBinding.wrappedValue
    }
    
    func deleteExpenseDidSwipe(indexSet: IndexSet) {
        day.expenses.remove(atOffsets: indexSet)
        let realmManager = RealmManager()
        realmManager.write(object: [day.toRealmModel()])
        dayBinding.wrappedValue = day
    }
    
    func expeseEdittingDoneButtonDidTap() {
        let realmManager = RealmManager()
        realmManager.write(object: [day.expenses[selectedExpenseIndex ?? 0].toRealmModel()])
        dayBinding.wrappedValue = day
        inputSummViewIsShown = false
    }
    
    func expenseDidSelect(expenseIndex: Int) {
        selectedExpenseIndex = expenseIndex
        inputSummViewIsShown = true
    }
}
