//
//  ExpensesListController.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 01.04.2021.
//

import SwiftUI

class ExpensesListController: UIHostingController<ExpensesListView> {
    
    init() {
        let viewModel = ExpensesListViewModel()
        super.init(rootView: ExpensesListView(viewModel: viewModel))
        viewModel.controller = self
    }
    
    @objc required dynamic init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // navigation
    func openAddExpenseController(monthsBinding: Binding<[Month]>) {
        let addExpenseController = AddExpenseController(monthsBinding: monthsBinding)
        let navigationController = UINavigationController(rootViewController: addExpenseController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    func openDayViewController(dayBinding: Binding<Day>) {
        let dayController = DayController(dayBinding: dayBinding)
        self.navigationController?.pushViewController(dayController, animated: true)
    }
}
