//
//  ExpensesListViewModel.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 26.04.2021.
//

import SwiftUI
class ExpensesListViewModel: ObservableObject {
    
    weak var controller: ExpensesListController?
    
    @Published var months: [Month] = []
    
    init() {
        let realmManager = RealmManager()
        
        if let realmExpenses = realmManager.obtain(objectType: MonthRealm.self) {
            months = Array(realmExpenses).map { $0.toPlainModel() }.sorted {
                $0.yearNumber != $1.yearNumber ? $0.yearNumber > $1.yearNumber : $0.monthNumber > $1.monthNumber
            }
        }
    }
    
    func addExpenseButtonDidTap(monthsBinding: Binding<[Month]>) {
        controller?.openAddExpenseController(monthsBinding: monthsBinding)
    }
    
    func dayDidSelect(dayBinding: Binding<Day>) {
        controller?.openDayViewController(dayBinding: dayBinding)
    }
}
