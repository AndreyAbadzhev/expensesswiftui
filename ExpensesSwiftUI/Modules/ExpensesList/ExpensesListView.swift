//
//  ExpensesListView.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 31.03.2021.
//

import SwiftUI

struct ExpensesListView: View {
        
    @ObservedObject var viewModel: ExpensesListViewModel
    
    var body: some View {
        VStack {
            
            // Спрятанный Divider нужен для того, чтобы ScrollView не скролился под прозрачный NavBar
            Divider().hidden()
            
            if viewModel.months.isEmpty {
                ExpensesListEmptyView(buttonDidTap: { viewModel.addExpenseButtonDidTap(monthsBinding: $viewModel.months) })
            } else {
                // Используем ScrollView вместо List потому что, как выяснилось, в swiftUI нельзя убрать сепаратор в List. Такая вот простая, но тупая фигня :)
                ScrollView(.vertical, showsIndicators: false) {
                    ForEach(viewModel.months.indices, id: \.self) { monthIndex in
                        DayExpensesSectionHeaderView(title: viewModel.months[monthIndex].monthName, totalSumm: viewModel.months[monthIndex].totalExpensesSumm)
                        ForEach(viewModel.months[monthIndex].days.indices, id: \.self) { dayIndex in
                            DayExpensesCell(day: viewModel.months[monthIndex].days[dayIndex]).onTapGesture {
                                viewModel.dayDidSelect(dayBinding: $viewModel.months[monthIndex].days[dayIndex])
                            }
                        }
                    }
                }
                .animation(.default)
            }
        }
        .animation(.default)
        
        // здесь задается задник для вьюхи. Туда передается отдельная вьюха, которая может иметь свои настройки цвета, фрейма и прочего, что очень удобно.
        .background(CommonViews.greenBackground())
        .navigationBarTitle("Расходы", displayMode: .inline)
        .navigationBarItems(trailing: Button(action: {
            viewModel.addExpenseButtonDidTap(monthsBinding: $viewModel.months)
        }, label: {
            Text("+").font(.system(size: 30))
                .foregroundColor(Colors.primaryTextColor)
        }))
    }
}
