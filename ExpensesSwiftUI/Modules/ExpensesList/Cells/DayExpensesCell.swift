//
//  DayExpensesListView.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 31.03.2021.
//

import SwiftUI

struct DayExpensesCell: View {    

    var day: Day
    
    var body: some View {
        VStack {
            Spacer(minLength: 8)
            HStack {
                Text(day.dateString)
                    .foregroundColor(Colors.secondaryTextColor)
                    .font(.system(size: 13))
                Spacer()
                Text(String(day.totalExpensesSumm))
                    .foregroundColor(Colors.secondaryTextColor)
                    .font(.system(size: 11))
                Text("₽")
                    .foregroundColor(Colors.secondaryTextColor)
                    .font(.system(size: 11))
                    .padding(EdgeInsets(top: 0, leading: -4, bottom: 0, trailing: 0))
            }
            Spacer(minLength: 12)
            ForEach(day.expensesReducedByCategories.indices, id: \.self) { index in
                ShowExpenseCell(expense: day.expensesReducedByCategories[index], isCommentAvailable: false)
            }
            Spacer(minLength: 8)
        }
        .padding(EdgeInsets(top: 0, leading: 32, bottom: 0, trailing: 32))
        .animation(.default)
        .background(RoundedCorners(color: .white, tl: 12, tr: 12, bl: 12, br: 12)
                        .shadow(color: Color.gray.opacity(0.3), radius: 3, x: 0, y: 5).padding(EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16)))
        Spacer(minLength: 8)
    }
}
