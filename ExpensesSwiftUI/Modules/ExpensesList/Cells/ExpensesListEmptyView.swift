//
//  ExpensesListEmptyView.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 29.04.2021.
//

import SwiftUI

struct ExpensesListEmptyView: View {

    var buttonDidTap: () -> Void
    
    var body: some View {
        VStack {
            Image(Images.handWithMoney)
                .resizable()
                .frame(width: 100, height: 100, alignment: .center)
            Text("У вас пока нет расходов")
                .foregroundColor(Colors.secondaryTextColor)
                .font(.system(size: 17))
                .padding(EdgeInsets(top: 12, leading: 0, bottom: 8, trailing: 0))
            Button("Добавить", action: { buttonDidTap() })
                .foregroundColor(Colors.primaryTextColor)
                .font(.system(size: 17))
        }
        .padding(EdgeInsets(top: -50, leading: 0, bottom: 0, trailing: 0))
    }
    
    init(buttonDidTap: @escaping () -> Void) {
        self.buttonDidTap = buttonDidTap
    }
}
