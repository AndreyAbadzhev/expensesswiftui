//
//  DayExpensesSectionHeaderView.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 01.04.2021.
//

import SwiftUI
struct DayExpensesSectionHeaderView: View {
    
    var title: String?
    var totalSumm: Int?
    
    var body: some View {
        VStack {
            if let title = self.title {
                Spacer().frame(height: 24)
                HStack {
                    Text(title)
                        .foregroundColor(Colors.secondaryTextColor)
                        .font(.system(size: 13, weight: .medium, design: .default))
                    Spacer()
                }
                .padding(EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16))
                Spacer().frame(height: 8)
            }
            
            if let totalSumm = self.totalSumm {
                HStack {
                    Text(String(totalSumm))
                        .foregroundColor(Colors.primaryTextColor)
                        .font(.system(size: 28, weight: .medium, design: .default))
                    Text("₽")
                        .foregroundColor(Colors.secondaryTextColor)
                        .font(.system(size: 28, weight: .medium, design: .default))
                        .padding(EdgeInsets(top: 0, leading: -4, bottom: 0, trailing: 0))
                    Spacer()
                }
                .padding(EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16))
                Spacer().frame(height: 12)
            }
        }
    }
}
