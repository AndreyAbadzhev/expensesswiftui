//
//  ExpenseView.swift
//  ExpensesSwiftUI
//
//  Created by Андрей Абаджев on 31.03.2021.
//

import SwiftUI

struct ShowExpenseCell: View {
    
    var expense: Expense
    
    var isCommentAvailable: Bool
    @State var isCommentShown = false
    
    var body: some View {
        VStack {
            HStack {
                
                Image(expense.category.imageName)
                    .resizable()
                    .frame(width: 22.0, height: 22.0)
                
                Spacer().frame(width: 16)
                
                Text(expense.category.name)
                    .foregroundColor(Colors.primaryTextColor)
                    .font(.system(size: 17))
                
                Spacer()
                
                if isCommentAvailable, !expense.comment.isEmpty {
                    Image(Images.comment).onTapGesture {
                        isCommentShown = !isCommentShown
                    }
                }
                
                Text(String(expense.summ))
                    .foregroundColor(Colors.primaryTextColor)
                    .font(.system(size: 15))
                
                Text("₽")
                    .foregroundColor(Colors.secondaryTextColor)
                    .font(.system(size: 15, weight: .light, design: .default))
                    .padding(EdgeInsets(top: 0, leading: -4, bottom: 0, trailing: 0))
            }
            .background(Color.clear)
            
            
            if isCommentShown {
                HStack {
                    Text(expense.comment)
                        .foregroundColor(Colors.secondaryTextColor)
                        .font(.system(size: 17))
                        .padding(EdgeInsets(top: 2, leading: 0, bottom: 0, trailing: 0)) // зачем-то нужен паддинг, чтобы комментарии при развороте показывали корректное количество строк. Иначе показывает только одну строку и linebreak
                    Spacer()
                }
            }
        }
    }
}
